# videotoannotation
[![pipeline status](https://gitlab.ethz.ch/chgerber/videotoannotation/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/videotoannotation/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/videotoannotation/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/videotoannotation/commits/master)

Leverage Google SpeechToText API to create video annotations.

### Requirements
Google SpeechToText API credentials
```bash
# Set env. variable with path to googleapi credentials:
GOOGLE_APPLICATION_CREDENTIALS="/home/christof/arnie-ac978346c655.json"
```

#### Convert video to lossless speech 
```cgo
ffmpeg -i Trump30Sec.mp4 -ac 1 Trump30SecMono.flac
```