module gitlab.ethz.ch/chgerber/videotoannotation

go 1.12

require (
	cloud.google.com/go v0.37.2
	github.com/stretchr/testify v1.3.0
	gitlab.ethz.ch/chgerber/MessageComposition v0.0.0-20190306170828-89650ee46910
	gitlab.ethz.ch/chgerber/annotation v0.0.0-20190524120442-d38aff855be5
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190527104216-9cd6430ef91e // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190525145741-7be61e1b0e51 // indirect
	google.golang.org/genproto v0.0.0-20190404172233-64821d5d2107
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
