package videotoannotation

import (
	"cloud.google.com/go/speech/apiv1"
	"context"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/annotation"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
	"io/ioutil"
)

// AudioToSpeach transcripts an audio file in flac format given by filename with the Google speech api
// !!!!This roughly costs 1 dollar per hour speech!!!!
func AudioToSpeach(filename string) ([]*speechpb.SpeechRecognitionResult, error) {
	ctx := context.Background()

	// Creates a client.
	client, err := speech.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	// Reads the audio file into memory.
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	// Detects speech in the audio file.
	resp, err := client.Recognize(ctx, &speechpb.RecognizeRequest{
		Config: &speechpb.RecognitionConfig{
			Encoding: speechpb.RecognitionConfig_FLAC,
			//SampleRateHertz: 16000,
			LanguageCode:          "en-US",
			EnableWordTimeOffsets: true,
		},
		Audio: &speechpb.RecognitionAudio{
			AudioSource: &speechpb.RecognitionAudio_Content{Content: data},
		},
	})
	if err != nil {
		return nil, err
	}

	return resp.Results, nil
}

func secAndNanosToMillies(sec int64, nanos int32) int {
	return int(sec*1e3) + int(nanos/1e6)
}

// WordToAnnotation creates annotations from google speech to test results
func WordToAnnotation(src string, sra *speechpb.SpeechRecognitionAlternative) (a []interface{}) {

	for i := range sra.Words {
		for wLength := 1; wLength <= 3; wLength++ {
			if i == len(sra.Words)-2 && wLength == 3 {
				continue
			}
			if i == len(sra.Words)-1 && wLength >= 2 {
				continue
			}
			words := sra.Words[i : i+wLength]
			startTime := words[0].StartTime
			endTime := words[wLength-1].EndTime
			var text []string
			for _, word := range words {
				text = append(text, word.Word)
			}
			newAnno := annotation.Annotation{
				Src: src,
				Subtitle: annotation.Subtitle{
					Count: i,
					Text:  util.WordsToString(text),
					Start: secAndNanosToMillies(startTime.Seconds, startTime.Nanos),
					End:   secAndNanosToMillies(endTime.Seconds, endTime.Nanos),
				},
			}
			a = append(a, newAnno)
		}
	}

	return a
}
