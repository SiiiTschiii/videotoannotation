package videotoannotation

import (
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	// Make sure ffmpeg binary exists
	if os.Getenv("GOOGLE_APPLICATION_CREDENTIALS") == "" {
		log.Fatalf("Env. variable GOOGLE_APPLICATION_CREDENTIALS not set")
	}
	_, err := os.Stat(os.Getenv("GOOGLE_APPLICATION_CREDENTIALS"))
	if err != nil {
		log.Fatal(err)
	}

	os.Exit(m.Run())
}

func TestSpeechToText(t *testing.T) {
	videoSrc := "videoTrump30SecMono.mp4"
	filename := "videoTrump30SecMono.flac"
	results, err := AudioToSpeach(filename)
	if err != nil {
		log.Fatal(err)
	}

	for _, result := range results {
		count := 0
		for _, sra := range result.Alternatives {
			annotations := WordToAnnotation(videoSrc, sra)
			count += len(annotations)
			assert.Equal(t, (len(sra.Words)-2)*3+3, count)
		}
	}
}
